//
//  PartyCell.swift
//  PartyRockApp
//
//  Created by Nuwan on 2016/10/08.
//  Copyright © 2016 Muse Pixel. All rights reserved.
//

import UIKit

class PartyCell: UITableViewCell {

    @IBOutlet weak var VideoPreviewImage: UIImageView!
    @IBOutlet weak var VideoTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func UpdateUI(partyRock: PartyRock){
        VideoTitle.text = partyRock.videoTitle
        //TODO: Set image from url
        let url = URL(string: partyRock.imageURL)!
        DispatchQueue.global().async {
            do {
                let data = try Data(contentsOf: url)
                DispatchQueue.global().sync {
                   
                    self.VideoPreviewImage.image = UIImage(data: data)
                   
                }
            }catch{
                //handel the error
            }
        }
    }
}
